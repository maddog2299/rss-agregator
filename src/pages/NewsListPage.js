'use strict';

import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NewsPreview from '../components/NewsPreview';
import {refreshChannel, readedChannel} from '../redux/actions/index';

class NewsListPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            id:'',
            activeChannel: {
                item:[]
            }
        }
    }

    componentDidMount(){
        let id = this.props.location.pathname.split('/')[2],
            activeChannel = this.props.chanels.chanels[id];
        this.props.readedChannel(activeChannel.srcUrl);
        this.setState({activeChannel, id});
    }

    componentWillReceiveProps(nextProps){
        let id = nextProps.location.pathname.split('/')[2],
            activeChannel = nextProps.chanels.chanels[id];
        this.setState({activeChannel, id});
    }

    refresh(event){
        event.preventDefault();
        this.props.refreshChannel(this.state.activeChannel.srcUrl);
    }


    render() {
        let NewsList =  this.state.activeChannel.item.map((news, i) => {
            return(
                <NewsPreview
                    key = {i}
                    id = {this.state.id}
                    number = {i}
                    descr = {news.description["#text"]}
                    title = {news.title["#text"]}
                    link = {news.link["#text"]}
                    pubDate = {news.pubDate["#text"]}
                />
            )
        })
        return(
            <section className="news-section">
                <div className="container">
                    <p className="refresh-news" onClick={(event)=>this.refresh(event)}>Refresh channel</p>
                    {NewsList}
                </div>
            </section>
            )
    }
}

function mapStateToProps(state) {
    return {
        chanels: state.chanels
    }
}

function mapDispatchToProps(dispatch) {
    return {
        refreshChannel: bindActionCreators(refreshChannel, dispatch),
        readedChannel: bindActionCreators(readedChannel, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsListPage)
