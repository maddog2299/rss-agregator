'use strict';

import React from 'react';
import SearchForm from '../components/SearchForm';
import ChannelPreview from '../components/ChannelPreview';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {AddChannel, refreshChannel, removeChannel} from '../redux/actions/index';


class ChannelListPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            chanels: [],
            pending: false
        }
    }


    componentDidMount(){
       //localStorage.removeItem('redux')
       let redux = localStorage.getItem('redux'),
           data = JSON.parse(redux);
       // debugger;
        if(data.chanels.chanels.length > 0){
            let chanels = data.chanels.chanels;
            this.setState({chanels})
        }
    }


    componentWillReceiveProps(nextProps) {
        let chanels  = nextProps.chanels.chanels,
            pending = chanels.pending;
        this.setState({chanels, pending})
    }

    render() {
        let chanelsList =  this.state.chanels.map((chanel, i) => {
            let image = chanel.image;
            if(image){
                image = image.url["#text"]
            }
            else{
                image=''
            }
            return(
                <ChannelPreview
                    key={i}
                    number={i}
                    descr={chanel.description["#text"]}
                    title={chanel.title["#text"]}
                    link={chanel.link["#text"]}
                    isNew={chanel.isNew}
                    imgSrc={image}
                    srcUrl={chanel.srcUrl}
                    refreshChannel={this.props.refreshChannel}
                    removeChannel={this.props.removeChannel}
                />
            )
        });

        return(
            <section className="main-section">
                { this.state.pending
                    ?
                        <p>Loading...</p>
                    :
                        <div className="home-wrapper">
                            <SearchForm
                                addChannel={this.props.addChannel}
                            />
                            { this.state.chanels.length > 0
                                ?
                            chanelsList
                                :
                                <h3>You don`t have any channels yet</h3>
                            }
                        </div>
                }
            </section>
            )
    }
}

function mapStateToProps(state) {
    return {
        chanels: state.chanels
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addChannel: bindActionCreators(AddChannel, dispatch),
        refreshChannel: bindActionCreators(refreshChannel, dispatch),
        removeChannel: bindActionCreators(removeChannel, dispatch)
    }
}

ChannelListPage.propTypes = {
    addChannel: React.PropTypes.func,
    chanels: React.PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(ChannelListPage)
