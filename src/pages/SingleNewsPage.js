/**
 * Created by Artyom on 04.03.2017.
 */

'use strict';

import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class SingleNewsPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            link: ""
        }
    }

    componentDidMount(){
        let channelId = this.props.location.pathname.split('/')[2],
            id = this.props.location.pathname.split('/')[3],
            link = this.props.chanels.chanels[channelId].item[id].link["#text"];
        this.setState({link});
    }


    render() {
        return(
            <section className="single-news">
                <iframe src={this.state.link} width="100%" height="800px"></iframe>
            </section>
            )
    }
}

function mapStateToProps(state) {
    return {
        chanels: state.chanels
    }
}

export default connect(mapStateToProps)(SingleNewsPage)
