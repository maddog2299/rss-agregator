'use strict'

import React from 'react';
import {render} from 'react-dom';
import {Router, Route} from 'react-router';
import Main from './components/MainComponent';
import {syncHistoryWithStore} from 'react-router-redux';
import {Provider} from 'react-redux';
import store from './redux/store';
import { hashHistory } from 'react-router';
import ChannelsListPage from './pages/ChannelListPage';
import NewsListPage from './pages/NewsListPage';
import SingleNewsPage from './pages/SingleNewsPage';
import ErrorPage from './pages/ErrorPage';

const history = syncHistoryWithStore(hashHistory, store);

render((
    <Provider store={store}>
        <Router history={history}>
            <Route name="home" component={Main}>
                <Route path="/" component={ChannelsListPage} />
                <Route path="/channel/:chanelId" component={NewsListPage} />
                <Route path="/channel/:chanelId/:newsId" component={SingleNewsPage} />
                <Route path="*" component={ErrorPage} />
             </Route>
        </Router>
  </Provider>
), document.getElementById('main'));