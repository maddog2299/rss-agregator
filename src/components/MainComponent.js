'use strict';
require('../styles/main.scss');

import React from 'react';
import 'jquery';
import 'bootstrap-loader';
import {connect} from 'react-redux';

class Main extends React.Component {

    constructor(props){
        super(props);
        this.state={}
    }

    componentWillReceiveProps(nextProps){

    }

    render() {
        return (
            <div>
                    <div>
                        <section className="content-section">
                            <div className="container">
                                <div className="col-md-12">
                                    {this.props.children}
                                </div>
                            </div>
                         </section>
                    </div>
            </div>
    );
  }
}

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(Main)
