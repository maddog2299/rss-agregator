/**
 * Created by Artyom on 05.03.2017.
 */
'use strict';
import React from 'react';
import {Link} from 'react-router';

export default class NewsPreview extends React.Component {
    render() {
        return(
            <div className="news-preview-wrapper">
                <Link className="channel-title" to={`/channel/${this.props.id}/${this.props.number}`}>{this.props.title}</Link>
                <p className="channel-descr">{this.props.descr}</p>
                <span>{this.props.pubDate}</span>
                <a href={this.props.link}>View on source</a>
            </div>
        )
    }
}