/**
 * Created by Artyom on 04.03.2017.
 */
import React from 'react';
import {Link} from 'react-router';

export default class ChannelPreview extends React.Component {

    constructor(props){
        super(props);
    }

    refresh(event){
        event.preventDefault();
        this.props.refreshChannel(this.props.srcUrl);
    }

    remove(event){
        event.preventDefault();
        this.props.removeChannel(this.props.srcUrl);
    }


    render(){
        let data = this.props;
        return(
                    <div className="preview-wrapper-big">
                        <div className="row">
                            <div className="col-sm-3 col-xs-12">
                                <div className="preview-part-wrapper">
                                    <img src={data.imgSrc} alt=""/>
                                    <a href={data.link}>View Source</a>
                                    {data.isNew &&
                                    <h2>
                                        You have unreaded news.
                                    </h2>
                                    }
                                </div>
                            </div>
                            <div className="col-sm-9 col-xs-12">
                                <div className="preview-part-wrapper-right">

                                    <p className="remove-channel" onClick = {(event)=>this.remove(event)}>Remove channel</p>
                                    <p className="refresh-channel" onClick = {(event)=>this.refresh(event)}>Refresh channel</p>
                                    <Link to={`/channel/${data.number}`} className="channel-title">{data.title}</Link>
                                    <span className="channel-descr">{data.descr}</span>
                                </div>
                            </div>
                        </div>
                    </div>
        )
    }
}