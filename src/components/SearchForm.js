/**
 * Created by Artyom on 04.03.2017.
 */
'use strict';
import React from 'react';

export default class SearchForm extends React.Component {

    constructor(props){
        super(props);
        this.state={
            source: ""
        }
    }

    formHandler(event){
        event.preventDefault();
        this.state.source.match('xml') ?
            this.props.addChannel(this.state.source) :
            alert("Cannot find RSS channel. Pls check the data type. It must be XML-RSS.");
        this.setState({
            source: ""
        })
    }

    handleChangeSource(event) {
        this.setState({source: event.target.value});
    }

    render(){
        return(
            <div className="preview-wrapper">
                <form onSubmit={(event)=>this.formHandler(event)} className="navbar-form" role="search">
                    <div className="form-group">
                        <input required value = {this.state.source} onChange={this.handleChangeSource.bind(this)} type="text" className="form-control" placeholder="Put rss channel" />
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>
        )
    }
}