'use strict';
import {xmlToJson} from './helper';
export default {

    fetch (conf) {
        return fetch(conf)
        .then(res => {
            if (res.ok) return res
            return res.text().then(text => {
                throw new Error('error ' + res.status + '\n' + text)
            })
        })
        .catch(err => {
            console.warn('api error: ' + err.message)
        })
    },

    checkError (res) {
        if (!res.ok) return res.text().then(text => {
            throw new Error('error ' + res.status + '\n' + text)
        });
        return res
    },

    addChannel (url) {
        return this
        .fetch(url)
        .then(response => response.text())
        .then(str => (new window.DOMParser()).parseFromString(str, "text/xml"))
        .then(data => xmlToJson(data))
        .catch(this.checkError)
    }
}