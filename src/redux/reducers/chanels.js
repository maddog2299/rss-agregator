'use strict';
import types from '../constants';

const initialState = {
    chanels: []
};

export default function chanels (state = initialState, action) {
    let payload = action.payload;
    switch (action.type) {
        case types.PENDING:
            return {...state, pending: true}
        case types.GET_CHANNEL_SUCCESS:
            let chanels = state.chanels;
            delete chanels.url;
            chanels.push(payload);
            return {...state}
        case types.REFRESH_CHANNEL_SUCCESS:
            let objIndex = state.chanels.findIndex((obj => obj.srcUrl == payload.srcUrl)),
                oldRead = state.chanels[objIndex].item[0].title;
            state.chanels[objIndex] = payload;
            let newRead = state.chanels[objIndex].item[0].title;
            if(oldRead.length < newRead.length){
                state.chanels[objIndex].isNew = true;
            }
            // debugger;
            return {...state}
        case types.REMOVE_CHANNEL_SUCCESS:
            state.chanels = state.chanels.filter(function(el){
                return el.srcUrl !== payload;
            });
            return {...state}
        case types.IS_NEW:
            let index = state.chanels.findIndex((obj => obj.srcUrl == payload));
            state.chanels[index].isNew = false;
            return {...state}
        case types.GET_CHANNEL_FAILED:
            return {...state, error: 'Cannot load channel'}
        case types.REFRESH_CHANNEL_FAILED:
            return {...state, error: 'Cannot refresh channel'}
        default:
            return state
    }
}

