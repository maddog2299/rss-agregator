'use strict';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import chanels from './chanels';

export default combineReducers({
    routing: routerReducer,
    chanels,
})
