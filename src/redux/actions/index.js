'use strict';

import types from '../constants';
import api from '../../api';

export function AddChannel (url) {
    return dispatch => {
        dispatch({type: types.PENDING});
        api.addChannel(url)
        .then(
            response => {
                response.rss.channel.srcUrl = url;
                dispatch({type: types.GET_CHANNEL_SUCCESS, payload: response.rss.channel})
            },
            error => {
                dispatch({type: types.GET_CHANNEL_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function refreshChannel (url) {
    return dispatch => {
        dispatch({type: types.PENDING});
        api.addChannel(url)
        .then(
            response => {
                response.rss.channel.srcUrl = url;
                dispatch({type: types.REFRESH_CHANNEL_SUCCESS, payload: response.rss.channel})
            },
            error => {
                dispatch({type: types.REFRESH_CHANNEL_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function removeChannel (srcUrl) {
    return {type: types.REMOVE_CHANNEL_SUCCESS, payload: srcUrl}
}

export function readedChannel (srcUrl) {
    return {type: types.IS_NEW, payload: srcUrl}
}