'use strict';

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers/index'
import persistState from 'redux-localstorage'

const store = createStore(reducer,
    compose(
        applyMiddleware(thunk),
        persistState()
    )
);

// store.subscribe(()=>{
//    console.log('!!', store.getState());
// });


export default store;