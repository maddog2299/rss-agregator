'use strict'

module.exports = {
    publicPath: '/public/',
    host: 'localhost',
    port: 8085
};